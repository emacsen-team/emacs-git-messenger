emacs-git-messenger (0.18-6) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:25:01 +0900

emacs-git-messenger (0.18-5) unstable; urgency=medium

  * Add upstream metadata
  * Install upstream changelog
  * Migrate to dh 13 without d/compat
  * d/control: Declare Standards-Version 4.5.1 (no changes needed)
  * d/control: Add Rules-Requires-Root: no
  * d/control: Drop emacs25 from Enhances
  * d/copyright: Bump copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 02 Jan 2021 13:08:35 +0500

emacs-git-messenger (0.18-4) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Thu, 29 Aug 2019 08:09:17 -0300

emacs-git-messenger (0.18-3) unstable; urgency=medium

  * Migrate to dh 11
  * Refresh patch
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org
  * d/control: Declare Standards-Version 4.1.4 (no changes needed)
  * d/control: Clean dependencies
  * d/control: Remove Built-Using field
  * d/control: Update Maintainer field (to Debian Emacsen team)

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 03 Jun 2018 23:07:54 +0500

emacs-git-messenger (0.18-2) unstable; urgency=medium

  * Fix debian/watch
  * Clear debian/control

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 18 Feb 2017 16:14:35 +0500

emacs-git-messenger (0.18-1) unstable; urgency=medium

  * Initial release (Closes: #854904)

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 11 Feb 2017 23:41:33 +0500
